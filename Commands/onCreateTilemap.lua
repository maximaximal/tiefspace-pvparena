function onCreateTilemap(args, player)
    player:msg("Tilemap Entity is creating...")
    e = CWorldLoop:createEntity()
    
    mapComponent = Component.Tilemap:new()
    e.addComponent(mapComponent)
    e.refresh()
    player:msg("Tilemap Entity created!")
    player:msg("The tilemap should be synchronized ")
end