-- Plugin Related Global Variables
    CWorldLoop = nil;
    CPlugin = nil;

-- Important Global Variables
    PVPMap = nil; -- The PVP Map
    
-- Initialize Function

function initialize(Plugin)
    CWorldLoop = Plugin:getWorldLoop()
    CPlugin = Plugin
    
    Plugin:registerChatCommand("createTilemap", "onCreateTilemap")
    
    initializePVPMap()
end